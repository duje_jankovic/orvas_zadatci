<?php
    class OrdersRepository {

        /**
         * @var PDO
         */
        private $db;

        public function __construct() {
            $this->db = new PDO('mysql:host=localhost;dbname=northwind;charset=utf8', 'root', '');
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        }

        /**
         * @param string $shipCity
         * @param string $shipName
         * @param string $orderDateFrom
         * @param string $orderDateTo
         * 
         * @return stdClass[]
         */
        function getOrders(string $shipCity = '', string $shipName = '', string $orderDateFrom = '', string $orderDateTo = '')
        {
            try {
                $parameters = [];
                $query = 'SELECT * FROM orders WHERE 1 = 1';
                if ('' !== $shipCity) {
                    $query .= " AND ship_city LIKE :ship_city";
                    $parameters[':ship_city'] = $shipCity.'%';
                }
                if ('' !== $shipName) {
                    $query .= " AND ship_name LIKE :ship_name";
                    $parameters[':ship_name'] = $shipName.'%';
                }
                if ('' !== $orderDateFrom) {
                    $query .= " AND order_date >= :order_date_from";
                    $parameters[':order_date_from'] = $orderDateFrom.'%';
                }
                if ('' !== $orderDateTo) {
                    $query .= " AND order_date <= :order_date_to";
                    $parameters[':order_date_to'] = $orderDateTo.'%';
                }
                $stmt = $this->db->prepare($query);
                $stmt->execute($parameters);
                return $stmt->fetchAll();
            } catch (PDOException $e) {
                return [];
            } 
        }
    }
?>