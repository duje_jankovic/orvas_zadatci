<!DOCTYPE html>
<?php
$errors = [];
include '../model/OrdersRepository.php';
try {
    $ordersRepository = new OrdersRepository();
} catch (PDOException $e) {
    $errors[] = 'Greska prilikom spajanja na bazu podataka';
}
$shipCity = $_POST['ship_city'] ?? '';
$shipName = $_POST['ship_name'] ?? '';
$orderDateFrom = $_POST['order_date_from'] ?? '';
$orderDateTo = $_POST['order_date_to'] ?? '';
if ([] === $errors) {
    $orders = $ordersRepository->getOrders($shipCity, $shipName, $orderDateFrom, $orderDateTo);
} else {
    $orders = [];
}
?>
<html>
    <head>
        <?php include 'head_info.html' ?>
        <title>Zadatak 1</title>
    </head>
    <body>
        <div class="container mt-5">
            <?php if ([] !== $errors): ?>
                <div class="row mb-5">
                    <div class="col-12 text-center" id="error-wrapper">
                        <?= printErrors($errors) ?>
                    </div>
                </div>
            <?php endif ?>
            <div class="row">
                <div class="col-12 text-center">
                    <form id="filter_orders_form" class="mt-2" action="" method="post" enctype="multipart/form-data">
                        Ship city:
                        <input class="my-2" size="25" type="text" name="ship_city" value="<?= $shipCity ?>"><br>
                        Ship name:
                        <input class="my-2" size="25" type="text" name="ship_name" value="<?= $shipName ?>"><br>
                        Order date from:
                        <input class="my-2" size="15" type="date" name="order_date_from" value="<?= $orderDateFrom ?>"> -
                        <input class="my-2" size="15" type="date" name="order_date_to" value="<?= $orderDateTo ?>"><br>
                        <input class="" type="submit" value="Filter">
                    </form>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12 m-2 text-center">
                    <h3>Rezultati:</h3><br>
                    <?php foreach ($orders as $order) : ?>
                        Ship city: <?= $order->ship_city ?>,
                        Ship name: <?= $order->ship_name ?>,
                        Ship date: <?= $order->order_date ?><br>
                    <?php endforeach ?>
                    <?php if (empty($orders)) : ?>
                        Nema rezultata
                    <?php endif ?>
                </div>
            </div>
        </div>
    </body>
</html>
<?php
function printErrors(array $errors)
{
    echo implode('<br>', $errors);
}
?>