<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Forms extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url'); 
        $this->load->library('grocery_CRUD');
 
    }
 
    public function customers()
    {
        $crud = new grocery_CRUD();
        $crud->set_subject('Customer');
        $crud->set_table('customers');
        $output = $crud->render();
 
        $this->generateForm($output);        
    }

    public function employees()
    {
        $crud = new grocery_CRUD();
        $crud->set_subject('Employee');
        $crud->set_table('employees');
        $crud->set_relation_n_n('privileges', 'employee_privileges', 'privileges', 'employee_id', 'privilege_id', 'privilege_name');
        $output = $crud->render();      
        $this->generateForm($output);        
    }

    private function generateForm($output = null)
 
    {
        $this->load->view('templates/header');
        $this->load->view('/pages/crudTemplate.php',$output); 
        $this->load->view('templates/footer');   
    }
}