
<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col-6 text-right">
                <button class="btn btn-primary"
                    onclick="window.location.href = '<?php echo site_url('forms/customers')?>';">Customers</button>
            </div>
            <div class="col-6 text-left">
                <button class="btn btn-primary"
                    onclick="window.location.href = '<?php echo site_url('forms/employees')?>';">Employees</button>
            </div>
        </div>          
    </div>
</body>

